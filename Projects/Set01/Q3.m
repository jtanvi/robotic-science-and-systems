% TODO: You write this function!
% input: f -> an 9-joint robot encoded as a SerialLink class
%        qInit -> 1x9 vector denoting current joint configuration
%        j3Target -> desired configuration for joint 3.
%        posGoal -> 3x1 vector denoting the target position to move to
% output: q -> 1x9 vector of joint angles that cause the end
%                     effector position to reach <position>
%                     (orientation is to be ignored)
function q = Q3(f,qInit,posGoal)

    qcurr = qInit;
    qconf = [1.5708, -0.3900, 0, 0.7850, 0, 0.7850, 0, -0.5000, 0.5000];
    alpha = 0.05;
    
    for i = 1:50
    
        % Calculating current position vector
        T = f.fkine(qcurr);
        x = T(1:3,4);
 
        % Error
        delta_x = alpha * (posGoal - x);

        % Jacobian Matrix 6x9
        Jo = f.jacob0(qcurr);

        % Jacobian Pseudoinverse 9x6
        Jinv = pinv(Jo);
        Jv = Jinv(:,1:3);
        
        % Adding Nullspace term
        nj = Jinv*Jo;
        q0dot = 0.0005 * (qconf - qcurr);
        N = (eye(9) - nj) * q0dot';

        % Calculating delta_q
        delta_q = (Jv * delta_x) + N;

        % Get joint angles
        q = delta_q' + qcurr;
        
        qcurr = q;
    end
   
end
    
    
