% TODO: You write this function!
% input: f1 -> an 9-joint robot encoded as a SerialLink class for one
%              finger
%        f2 -> an 9-joint robot encoded as a SerialLink class for one
%              finger
%        qInit -> 1x11 vector denoting current joint configuration.
%                 First six joints are the arm joints. Joints 8,9 are
%                 finger joints for f1. Joints 10,11 are finger joints
%                 for f2.
%        f1Target, f2Target -> 3x1 vectors denoting the target positions
%                              each of the two fingers.
% output: q -> 1x11 vector of joint angles that cause the fingers to
%              reach the desired positions simultaneously.
%              (orientation is to be ignored)
function q = Q4(f1,f2,qInit,f1Target,f2Target)

    qcurr1 = qInit(1,1:9);                       % 1x9 vector
    qcurr2 = [qInit(1,1:7) qInit(1,10:11)];      % 1x9 vector
    alpha = 0.05;
    
    for i = 1:100
    
        % Calculating end-effector current position for f1
        T1 = f1.fkine(qcurr1);
        x1 = T1(1:3,4);
        
        % Calculating end-effector current position for f1
        T2 = f2.fkine(qcurr2);
        x2 = T2(1:3,4);

        % Error
        delta_x1 = alpha * (f1Target - x1);
        delta_x2 = alpha * (f2Target - x2);

        % Jacobian J1 and J2 6x9 for f1 and f2
        J1 = f1.jacob0(qcurr1);  
        J2 = f2.jacob0(qcurr2);
        J = [J1 J2(1:6,8:9)];     

        % Pinv Jacobian 6x9 for both
        Jinv = pinv(J);           
        
        %Extracting Jacobian p-inverse corresponding to linear velocity from Jinv's
        Jv = Jinv(:,1:3);         

        % Calculating delta_q for both f1 and f2
        Jv1 = Jv(1:9,:);
        Jv2 = vertcat(Jv(1:7,:), Jv(10:11,:));
        delta_q1 = Jv1 * delta_x1;
        delta_q2 = Jv2 * delta_x2;

        % Get joint angles
        q1 = delta_q1' + qcurr1; % ---1x9 vector
        q2 = delta_q2' + qcurr2; % ---1x9 vector
        
        qcurr1 = q1;
        qcurr2 = [q1(1,1:7) q2(1,8:9)];
    end
    
    q = [qcurr1 qcurr2(1,8:9)];

end

    
