% Calculate a path from qStart to xGoal
% input: qStart -> 1x4 joint vector describing starting configuration of
%                   arm
%        xGoal -> 3x1 position describing desired position of end effector
%        sphereCenter -> 3x1 position of center of spherical obstacle
%        sphereRadius -> radius of obstacle
% output -> qMilestones -> 4xn vector of milestones. A straight-line interpolated
%                    path through these milestones should result in a
%                    collision-free path. You may output any number of
%                    milestones. The first milestone should be qStart. The
%                    last milestone should place the end effector at xGoal.
function qMilestones = Q2(rob,sphereCenter,sphereRadius,qStart,xGoal)

    %% Generating the Tree

    Tgoal = transl(xGoal);
    qGoal = rob.ikine(Tgoal, qStart, [1 1 1 0 0 0]);
    
    % Initializing the Tree
    Tree = [qStart 0];
    maxIterLimit = 1000;
    
    for i = 1:maxIterLimit
        
        rand_num = rand(1,1);
        if rand_num < 0.1
            qRand = [qGoal 0];  
        else
            qRand = [rand(1,4)*2*pi - pi 0];
        end
        
        nodeNear = knnsearch(Tree(:,1:4), qRand(1,1:4));
        qNear = Tree(nodeNear,:);
        qNew = extend(qNear, qRand, rob, sphereCenter,sphereRadius, nodeNear);
        Tree = [Tree; qNew];
        
    end 
    
    %% Computing Path starting from qGoal to qStart using the generated Tree
    
    % Find the qGoal configuration in the Tree
    find_node = knnsearch(Tree(:,1:4), qGoal);
    qAdd = Tree(find_node,:);
    qGoal0 = [qGoal 0];
    
    % Initialize and add qGoal to Path
    Path = [qGoal];
    new_node = qAdd(1,5);
    
    while true
        q = Tree(new_node,:);
        q1 = q(1,1:4);    % next node to be added on the path
        q2 = Path(1,:);   % first node on the path
        % collision check -- if there is any, between the nodes of the path
        collision = Q1(rob,q1,q2,sphereCenter,sphereRadius);
        
        % Index of the next node to be added on the Path
        new_node = q(1,5);
        
        if collision == 0 
            Path = [q(1,1:4); Path];              
        else
            Tree = [Tree(1:prev_node-1,:); Tree(prev_node+1:end, :)];
            new_node = knnsearch(Tree(:,1:4), q2);
        end
        
        if new_node == 0
            break;
        end
    end
    
    qMilestones = Path;
  
end


%% Helper Function 'Extend' for finding a new node close to the qRandom_Config()

function qNew = extend(qNear, qRand, rob, c, rad, nodeNear)

    flag = false;
    alpha = 0.0;
    q1 = qNear(1,1:4);
    q2 = qRand(1,1:4);
    
    for i= 1:10
        
        alpha = alpha + 0.1;
        q = q1*(1-alpha) + alpha*q2;
        col = robotCollision(rob,q,c,rad);
        
        if col == 1
            flag = true;
            break;
        end
    end
    
    if flag == true
        alpha = alpha - 0.1;
        qNew = q1*(1-alpha) + alpha*q2;
        qNew = [qNew nodeNear];
    else
        qNew = [q2 nodeNear];
    end  

end



