% Localize a cylinder in the point cloud. Given a point cloud as input, this
% function should locate the position and orientation, and radius of the
% cylinder.
% input: ptCloud -> a pointCloud object that contains the point cloud (see
%                   Matlab documentation)
% output: center -> 3x1 vector denoting cylinder center
%         axis -> 3x1 unit vector pointing along cylinder axis
%         radius -> scalar radius of cylinder
function [center,axis,radius] = Q2(ptCloud)

    max = -1;
    center = [0 0 0];
    radius = 0.05;

    for i = 1:100
        
        % Sample Radius
        currRadius = (0.11 - 0.05) .* rand() + 0.05;

        % Sample Two Points from the point cloud
        n = ptCloud.Count();
        randNum = randi(n,1,2);
        a = ptCloud.Location(randNum(1,1),:);
        b = ptCloud.Location(randNum(1,2),:);

        % Get the surface normals
        normals = pcnormals(ptCloud);
        a_normal = normals(randNum(1,1),:);
        b_normal = normals(randNum(1,2),:);

        % Get the axis of the cylinder
        axis = (cross(a_normal, b_normal))';
        axis(1,1) = 0;
        axis(2,1) = 0;

        % Project the points on the plane orthogonal to the axis
        points = ptCloud.Location();
        x_plane = (eye(3) - (axis * axis')) * points';

        % Select candidate center
        cand_center = a + (a_normal * currRadius);

        % Project candidate center on the plane orthogonal to the axis 
        currCenter = ((eye(3) - (axis' * axis)) * cand_center')';

        % Score the Circle using the projected points and the sampled
        % center and radius
        count = fitCircle(x_plane', currCenter, currRadius);

        if count > max
            max = count;
            center = currCenter';
            radius = currRadius';
        end
    
    end
    
end


function count = fitCircle(locs, c, r)

    % Epsilon
    e = 0.01;
    
    % nx1 distance matrix where every element correspond to the distance between every point and the center
    dist = pdist2(locs, c);             
    
    % this matrix consists of points that lie close to the circumference of the circle of given center and radius
    points_within = dist(dist >= (r - e) & dist <= (r + e));  
    
    % Total number of points in the vector above
    count = size(points_within,1);
    
end