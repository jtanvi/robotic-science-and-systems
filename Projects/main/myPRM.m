

function prmPath = myPRM(robot, sphereCenter, sphereRadius, qStart, xGoal)

    Tgoal = transl(xGoal);
    qGoal = robot.ikine(Tgoal, qStart);


    %% GENERATE ROADMAP
    
    % roadmap G = (V, E) where V is a robot configuration sampled randomly
    % and E are the edges
    
    Tree = [];
    flag = true;
    roadMap = graph();
    %W = roadMap.Edges.Weight;
    %roadmap(:,1)
    maxIter = 100;
    node = 0;
    var = 1;
    qfinal = [];
    for i = 1:maxIter
    
        while var == 1
            qRand = [rand(1,6)*2*pi - pi];
            if robotCollision(robot, qRand, sphereCenter, sphereRadius) == 0
                break;
            end
        end
        
        node = node + 1;
        Tree = [Tree; qRand];
        roadMap = addnode(roadMap,node);
        
        [qNeighbors, dist] = knnsearch(Tree(1:end-1,:), qRand, 'k', 5);
        [n, m] = size(qNeighbors);
        %[mat, dist] = knnsearch(Tree(1:end-1,:), qRand, 'k', 3)
        for j = 1:m
            qNear = Tree(qNeighbors(1,j),:);
            %neighbor_num = qNeighbors(1,j);
            if or((Q1(robot, qRand, qNear, sphereCenter, sphereRadius) == 1), (qNear == qRand))
                %disp('clause 1')
                continue;
            else  
                roadMap = addedge(roadMap, node, qNeighbors(1,j));
                %roadMap.Edges.Weight(end+1) = dist(1,j);
            end
        end       
    end
    
   % figure
   % plot(roadMap)
   % roadMap.Edges    %-- get the edges in the graph
    %Tree  
    
    %% QUERY PROCESSING
    %find shortest path in the generated roadmap
    start_index = knnsearch(Tree,qStart);
    final_index = knnsearch(Tree,qGoal);
    shortest_path = shortestpath(roadMap, start_index, final_index); 
    
    %size(shortest_path)
    for k = 1 : (length(size(shortest_path)))
        qfinal = [qfinal; Tree(shortest_path(k),:,:)];
    end
    path =  [qStart; qfinal; qGoal];
    prmPath = path
end