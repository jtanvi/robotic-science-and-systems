
%% MAIN

function proj(questionNum)

     close all;
    
    if nargin < 1
        error('Error: please enter a question number as a parameter');
%         questionNum = 2;
    end
    
    % set up robot and initial joint configuration
	% rob = createRobot();
	mdl_puma560;
    qStart = [0    0.7854    3.1416         0    0.7854         0];
	xGoal = [0.5;0.0;0.5];
    sphereCenter = [0.5;0.;-0.25];
    sphereRadius = 0.2;
    
    % plot robot and sphere
    p560.plot(qStart);
    hold on;	
    drawSphere(sphereCenter,sphereRadius);
    
    
    qMilestones = myPRM(p560,sphereCenter,sphereRadius,qStart,xGoal);

    % interpolate and plot direct traj from start to goal
    qTraj = interpMilestones(qMilestones);
    p560.plot(qTraj);
    

end

%% TRAJ

function traj = interpMilestones(qMilestones)

    d = 0.05;
%     traj = qMilestones(1,:);
    traj = [];
    for i=2:size(qMilestones,1)
        
        delta = qMilestones(i,:) - qMilestones(i-1,:);
        m = max(floor(norm(delta) / d),1);
        vec = linspace(0,1,m);
        leg = repmat(delta',1,m) .* repmat(vec,size(delta,2),1) + repmat(qMilestones(i-1,:)',1,m);
        traj = [traj;leg'];
        
    end
end

%% GET PATH

function qPath = getPath(tree)

    m = 10;
    idx = size(tree,1);
    path = tree(end,1:end-1);
    
    while(idx ~= 1)
        
        curr = tree(idx,1:end-1);
        idx = tree(idx,end);
        next = tree(idx,1:end-1);
        path = [path;[linspace(curr(1),next(1),m)' linspace(curr(2),next(2),m)' linspace(curr(3),next(3),m)' linspace(curr(4),next(4),m)']];
        
    end
    qPath = path(end:-1:1,:);
    
end

%% DRAW SPHERE

function drawSphere(position,diameter)

%     diameter = 0.1;
    [X,Y,Z] = sphere;
    X=X*diameter;
    Y=Y*diameter;
    Z=Z*diameter;
    X=X+position(1);
    Y=Y+position(2);
    Z=Z+position(3);
    surf(X,Y,Z);
    %~ shading flat

end
