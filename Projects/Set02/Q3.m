% Smooth path given in qMilestones
% input: qMilestones -> nx4 vector of n milestones. 
%        sphereCenter -> 3x1 position of center of spherical obstacle
%        sphereRadius -> radius of obstacle
% output -> qMilestones -> 4xm vector of milestones. A straight-line interpolated
%                    path through these milestones should result in a
%                    collision-free path. You should output a number of
%                    milestones m<=n.
function qMilestonesSmoothed = Q3(rob,qMilestones,sphereCenter,sphereRadius)

    qStart = qMilestones(1,:);
    n = size(qMilestones, 1);
    
    % Initialize q1 and q2
    q1 = qStart;
    q2 = qMilestones(n,:);
    counter = 2;
    Path = [q2];
    
    while q1 ~= q2  % Until the entire Path is traversed
    
        collision = Q1(rob,q1,q2,sphereCenter,sphereRadius);
        
        if collision == 0
            Path = [q1; Path];
            q2 = q1;
            counter = 1;
        end
        
        q1 = qMilestones(counter,:);
        counter = counter + 1;
        
    end
    
    Path = [qStart; Path];
    qMilestonesSmoothed = Path;

end
