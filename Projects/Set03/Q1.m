% Localize a sphere in the point cloud. Given a point cloud as input, this
% function should locate the position and radius of a sphere.
% input: ptCloud -> a pointCloud object that contains the point cloud (see
%                   Matlab documentation)
% output: center -> 3x1 vector denoting sphere center
%         radius -> scalar radius of sphere
function [center,radius] = Q1(ptCloud)

    max = -1;
    center = [0 0 0];
    radius = 0.05;
    
    for j = 1:100

        % Sample Point
        n = ptCloud.Count();
        randNum = randi(n,1,1);
        a = ptCloud.Location(randNum,:);

        % Get the normals on all points of the point cloud    
        normals = pcnormals(ptCloud);
        a_normal = normals(randNum, :);
        
        % Sample Radius
        currRadius = (0.11 - 0.05) .* rand() + 0.05;

        % Get the center from the Sampled Point
        currCenter = a + (a_normal .* currRadius);
  
        % Sphere Score
        count = points_count(ptCloud, currCenter, currRadius);
          
        if count > max
            max = count;
            center = currCenter;
            radius = currRadius;
        end
             
    end  
        
end


function count = points_count(ptCloud, c, r)

    % Epsilon
    e = 0.02;
    
    % nx3 matrix of X,Y,Z coordinates of the points in the point cloud
    locs = ptCloud.Location(); 
    
    % nx1 distance matrix where every element correspond to the distance between every point and the center
    dist = pdist2(locs, c);             
    
    % this matrix consists of points that lie close to the surface of the sphere of given center and radius
    points_within = dist(dist >= (r - e) & dist <= (r + e));  
    
    % Total number of points in the vector above
    count = size(points_within,1);

end

