% 
% This function takes two joint configurations and the parameters of the
% obstacle as input and calculates whether a collision free path exists
% between them.
% 
% input: q1, q2 -> start and end configuration, respectively. Both are 1x4
%                  vectors.
%        sphereCenter -> 3x1 position of center of sphere
%        r -> radius of sphere
%        rob -> SerialLink class that implements the robot
% output: collision -> binary number that denotes whether this
%                      configuration is in collision or not.
%                       1 --> collision
%                       0 --> no collision
function collision = Q1(rob,q1,q2,sphereCenter,r)

    flag = false;
    alpha = 0.0;
    
    for i= 1:10
        
        alpha = alpha + 0.1;
        q = q1*(1-alpha) + alpha*q2;
        col = robotCollision(rob,q,sphereCenter,r);
        
        if col == 1
            flag = true;
            break;
        end
    end
    
    if flag == false
        collision = 0;
    else
        collision = 1;
    end      
        
end

